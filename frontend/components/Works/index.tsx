import style from './Works.module.scss'
function WorksComponent({ work }) {
  return (
    <>{work && work.map((job) => <WorkPost key={job.name} work={job} />)}</>
  )
}

function WorkPost({ work }) {
  return (
    <div className={style.workContainer}>
      <div className={style.workInfo}>
        <div className={style.workInfoLeft}>
          <div className={style.workProfile}>
            <img src={work.logo} alt={work.title} />
          </div>
          <div>
            <div className={style.title}>
              <h2 className={style.h2}>{work.title}</h2>
            </div>
            <div className={style.subTitle}>
              <h3 className={style.h3}>{work.subtitle}</h3>
            </div>
          </div>
        </div>
        <div className={style.workInfoRight}>
          <a target="_blank" rel="noreferrer" href={work.link}>
            <div className={style.workInfoBtn}>
              <span>Ver trabajo</span>
            </div>
          </a>
        </div>
      </div>
      <div className={style.workImage}>
        <img src={work.image} alt="" />
      </div>
      <div className={style.workHow}>
        {work.design && (
          <div className={style.workDesign}>
            <Button type="design">{work.design.map((des) => des)}</Button>
            <p className={style.text}>Figma</p>
          </div>
        )}
        {work.development && (
          <div className={style.workDev}>
            <Button type="development">Desarrollo</Button>
            <p className={style.text}>{work.development.map((dev) => dev)}</p>
          </div>
        )}
      </div>
    </div>
  )
}

function Button({ children, type }) {
  const styleType = type === 'design' ? style.design : style.development
  return (
    <button className={`${style.button} ${styleType}`}>
      <span>{children}</span>
    </button>
  )
}
export default WorksComponent
