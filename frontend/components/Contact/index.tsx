import { Github, Gitlab, Linkedin } from '@/components/SVG'
import style from './Contact.module.scss'

function Contacto() {
  return (
    <div className={style.contact}>
      <div className={style.image}>
        <img src="mati.jpeg" alt="Matias Caballero foto de perfil" />
      </div>
      <div className={style.findMe}>
        <div className={style.findMeSpan}>
          <span>Encuentrame en</span>
        </div>
        <div className={style.socialMedia}>
          <div>
            <a
              target="_blank"
              rel="noreferrer"
              href="https://matiascaballero.page.link/linkedin"
            >
              <Linkedin />
            </a>
          </div>
          <div>
            <a
              target="_blank"
              rel="noreferrer"
              href="https://matiascaballero.page.link/gitlab"
            >
              <Gitlab />
            </a>
          </div>
          <div>
            <a
              target="_blank"
              rel="noreferrer"
              href="https://matiascaballero.page.link/github"
            >
              <Github />
            </a>
          </div>
        </div>
      </div>
      <div className={style.talk}>
        <div className={style.talkSpan}>
          <span>¿Quéres que hablemos?</span>
        </div>
        <div className={style.talkButton}>
          <a href="mailto:contacto@matiascaballero.com">
            <button>Enviar Mensaje</button>
          </a>
        </div>
      </div>
    </div>
  )
}
export default Contacto
