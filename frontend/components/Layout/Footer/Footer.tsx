import Link from 'next/link'
import { useRouter } from 'next/router'
import { ContactOutline, MeOutline, WorkOutline } from '@/components/SVG'

import style from './Footer.module.scss'

function Footer() {
  const router = useRouter()
  return (
    <div className={style.footer}>
      <Link href="/">
        <a>
          <div
            className={`${style.aboutMe} ${
              router.pathname === '/' ? `${style.active}` : ''
            }`}
          >
            <MeOutline />
          </div>
        </a>
      </Link>
      <Link href="/works">
        <a>
          <div
            className={`${style.works} ${
              router.pathname === '/works' ? `${style.active}` : ''
            }`}
          >
            <WorkOutline />
          </div>
        </a>
      </Link>
      <Link href="/contact">
        <a>
          <div
            className={`${style.contact} ${
              router.pathname === '/contact' ? `${style.active}` : ''
            }`}
          >
            <ContactOutline />
          </div>
        </a>
      </Link>
    </div>
  )
}

export default Footer
