import { useTitle } from '@/components/Context/SectionContext'
import style from './Header.module.scss'

function Header() {
  const { title } = useTitle()
  return (
    <div className={style.header}>
      <span className={style.logo}>{title}</span>
    </div>
  )
}

export default Header
