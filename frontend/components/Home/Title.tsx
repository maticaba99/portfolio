import style from './Home.module.scss'

function Title() {
  return (
    <div className={style.title}>
      <div className={style.image}>
        <img src="mati.jpeg" alt="Matias Caballero foto de perfil" />
      </div>
      <div className={style.titleContent}>
        <div>
          <h1 className={style.h1}>Matias Caballero</h1>
        </div>
        <div>
          <h2 className={style.h2}>
            Co-Founder de &nbsp;
            <span
              data-tooltip="Directorio de ONG's sin animo de lucro"
              className={style.cofound}
            >
              Gentem &nbsp;
            </span>
            |
            <span data-tooltip="Agencia creativa" className={style.cofound}>
              &nbsp; KABÜPER &nbsp;
            </span>
          </h2>
          <h3 className={style.h3}>Desarrollador web fullstack</h3>
        </div>
      </div>
    </div>
  )
}

export default Title
