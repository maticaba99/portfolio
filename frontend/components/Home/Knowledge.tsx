import style from './Home.module.scss'
function Knowledge() {
  return (
    <div className={style.knowledge}>
      <div className={style.knowledgeTitle}>
        <h4 className={style.h4}>Conocimientos</h4>
      </div>
      <KnowledgeContainer title="Programación" text="Javascript | Typescript" />
      <KnowledgeContainer
        title="Librerias, Frameworks y más"
        text="React.js | Next.js | SCSS"
      />
      <KnowledgeContainer
        title="Base de datos y servicios"
        text="MongoDB | GraphQL | AWS | CI/CD"
      />
      <KnowledgeContainer title="Diseño" text="Figma | Photoshop" />
      <KnowledgeContainer title="Marketing Digital" text="SEO | Google Ads" />
    </div>
  )
}

function KnowledgeContainer({ title, text }) {
  return (
    <div className={style.knowledgeContainer}>
      <div className={style.knowContainerTitle}>
        <h5 className={style.h5}>{title}</h5>
      </div>
      <div>
        <p className={style.text}>{text}</p>
      </div>
    </div>
  )
}

export default Knowledge
