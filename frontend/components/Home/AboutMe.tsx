import style from './Home.module.scss'
function AboutMe() {
  return (
    <div className={style.aboutMe}>
      <div className={style.aboutMeTitle}>
        <h4 className={style.h4}>Sobre mi</h4>
      </div>
      <div>
        <p className={style.text}>
          Desde pequeño me sentí unido a la tecnología, desde los videojuegos
          hasta diseñar en Photoshop y Cinema4D para usuarios de Taringa.
          Actualmente me dedico al desarrollo de aplicaciones web aunque hago
          diseño y marketing digital como hobby.
        </p>
      </div>
    </div>
  )
}

export default AboutMe
