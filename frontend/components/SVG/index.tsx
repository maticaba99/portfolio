import ContactOutline from './ContactOutline'
import MeOutline from './MeOutline'
import WorkOutline from './WorkOutline'
import Gitlab from './Gitlab'
import Github from './Github'
import Linkedin from './Linkedin'

export { ContactOutline, MeOutline, WorkOutline, Gitlab, Github, Linkedin }
