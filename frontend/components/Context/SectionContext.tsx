import { createContext, useContext, Dispatch, SetStateAction } from 'react'
interface titleContext {
  title: string
  setTitle: Dispatch<SetStateAction<string>>
}

const titleContext = createContext<titleContext>({
  title: 'General',
  // eslint-disable-next-line @typescript-eslint/no-empty-function
  setTitle() {},
})

export const useTitle = () => useContext(titleContext)
export const TitleProvider = titleContext.Provider
