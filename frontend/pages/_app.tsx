import { useState } from 'react'
import Head from 'next/head'
import Layout from '@/components/Layout'
import { TitleProvider } from '@/components/Context/SectionContext'
import '../styles/globals.scss'

function MyApp({ Component, pageProps }) {
  const [title, setTitle] = useState('Inicio')
  return (
    <>
      <Head>
        <title>Matias Caballero | Desarrollador fullstack MERN</title>
        <meta
          name="description"
          content="Matias Caballero, web developer profesional en MERN y Jamstack"
        />
      </Head>
      <TitleProvider value={{ title, setTitle }}>
        <Layout>
          <div id="__webContainer">
            <Component {...pageProps} />
          </div>
        </Layout>
      </TitleProvider>
    </>
  )
}

export default MyApp
