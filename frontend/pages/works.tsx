import Head from 'next/head'
import WorksComponent from '@/components/Works'
import { useTitle } from '@/components/Context/SectionContext'

const fakeApi = [
  {
    logo: 'gentem.jpeg',
    title: 'Gentem',
    subtitle: 'Directorio de Ongs',
    link: 'https://gentem.org/',
    image: 'gentemScreen.png',
    development: ['Nextjs |', ' MongoDB |', ' AWS |', ' SASS'],
  },
  {
    logo: 'laisabella.jpg',
    title: 'La Isabella',
    subtitle: 'E-Commerce',
    link: 'https://laisabellabagtobag.com',
    image: 'laisabella.png',
    design: ['Figma'],
    development: ['Nextjs |', ' MongoDB |', ' AWS |', ' SASS'],
  },
]

function Works() {
  const { setTitle } = useTitle()
  setTitle('Ultimos trabajos')
  return (
    <>
      <Head>
        <title>Matias Caballero | Ultimos trabajos</title>
      </Head>
      <WorksComponent work={fakeApi} />
    </>
  )
}

export default Works
