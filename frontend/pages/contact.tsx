import Head from 'next/head'
import Contacto from '@/components/Contact'
import { useTitle } from '@/components/Context/SectionContext'

function Contact() {
  const { setTitle } = useTitle()
  setTitle('Contacto')
  return (
    <>
      <Head>
        <title>Matias Caballero | Contacto</title>
      </Head>
      <Contacto />
    </>
  )
}

export default Contact
