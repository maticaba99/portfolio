import { useTitle } from '@/components/Context/SectionContext'
import Title from '@/components/Home/Title'
import AboutMe from '@/components/Home/AboutMe'
import Knowledge from '@/components/Home/Knowledge'

export default function Home() {
  const { setTitle } = useTitle()
  setTitle('Inicio')
  return (
    <>
      <Title />
      <AboutMe />
      <Knowledge />
    </>
  )
}
